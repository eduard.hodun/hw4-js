    Теоретичні питання:

    1.Описати своїми словами навіщо потрібні функції у програмуванні.
        В JS функції необхідні щоб не повторювати той самий код у багатьох місцях. 
        Функції є основними "будівельними блоками" програми.

    2.Описати своїми словами, навіщо у функцію передавати аргумент.
        Для того щоб функція працювала динамічно та надавала різний результат, їй потрібні дані - аргументи.
        Аргументи вказуються при виклику функції, після чого вони йдуть у параметри функціїї та 
        використовуються впродовж всієї функції.

    3.Що таке оператор return та як він працює всередині функції?
        Оператор return - це оператор, який повертає результат виконання функції.
        Без явного вказання оператора return, функція поверне undifind. Тому, якщо ми хочемо отримати повноцінну функцію,
        з певним результатом нам обовязково необхідно вказати оператор return.



Завдання
Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
1.Отримати за допомогою модального вікна браузера два числа.

2.Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. 
Сюди може бути введено +, -, *, /. 

3.Створити функцію, в яку передати два значення та операцію.

4.Вивести у консоль результат виконання функції.


Необов'язкове завдання підвищеної складності
Після введення даних додати перевірку їхньої коректності. 
Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову 
(при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).


let firstUserNumber = +prompt('Введіть своє число: ' , '');

while (isNaN(firstUserNumber) || firstUserNumber === 0){
alert("Введіть своє число ще раз:")
firstUserNumber = +prompt('Введіть своє число ще раз: ', '');
}
console.log(firstUserNumber);





let secondUserNumber = +prompt('Введіть своє число: ' , '');

while (isNaN(secondUserNumber) || secondUserNumber === 0){
alert("Введіть своє число ще раз:")
secondUserNumber = +prompt('Введіть своє число ще раз: ', '');
}
console.log(secondUserNumber);




let mathOperation = +prompt('Введіть свою математичну операцію: ' , '');

// while (isNaN(mathOperation) || mathOperation === 0 || mathOperation === "" || Number(mathOperation)){
//     alert("Введіть свою математичну операцію ще раз:")
//     mathOperation = +prompt('Введіть свою математичну операцію ще раз: ', '');
// }
// console.log(mathOperation);
//
// while (mathOperation !== "-" || mathOperation !== "+" || mathOperation !== "/" || mathOperation !== "*"){
//     alert("Введіть свою математичну операцію ще раз:")
//     mathOperation = +prompt('Введіть свою математичну операцію ще раз: ', '');
// }

console.log(mathOperation);

// const  doMathOperation = (number1, number2, a) => {
//
//     return (number1 a number2);
// }

const calc = function(a, b, math) {
let sum = 0;
sum = (a) (math) (b);
return sum;
}

console.log(calc(firstUserNumber, secondUserNumber, mathOperation));